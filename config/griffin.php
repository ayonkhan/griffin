<?php

return [

    /*
     * Griffin service is disable unless it is set to true.
     */
    'enabled' => env('GRIFFIN_ENABLED', false),

    /*
     * This is the name of the table that will be created by the migration and
     * used by the Griffin model shipped with this package.
     */
    'table_name' => '_vendor_griffin',

    /*
     * This is base url of Shohoz Cloud Accounting System.
     */
    'base_url' => env('GRIFFIN_SERVICE_BASE_URL'),

    /*
     * This is the name of the vertical of Shohoz like food, ride, truck etc.
     */
    'source_name' => env('GRIFFIN_SOURCE'),

    /*
     * This is the name of the database connection to store the transactional data.
     */
    'database_connection_name' => 'griffin-mongodb',

    /*
     * This is the db connection name to store the transactional logs of
     * Shohoz Cloud Accounting System.
     */
    'mongodb' => [
        'driver' => 'mongodb',
        'dsn' => env('GRIFFIN_MONGO_DB_DSN'),
        'database' => env('GRIFFIN_MONGO_DB_DATABASE')
    ],
];
