<?php

namespace Shohoz\Griffin\Accounts;

use Shohoz\Griffin\Contracts\AccountContract;

abstract class Account implements AccountContract
{
    private $amount;

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public abstract function getAccountId();

    public function getDisplayName()
    {
        return (new \ReflectionClass(static::class))->getShortName();
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function toArray()
    {
        return [
            'AccountId' => $this->getAccountId(),
            'Amount' => $this->amount
        ];
    }
}
