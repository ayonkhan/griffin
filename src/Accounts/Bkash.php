<?php

namespace Shohoz\Griffin\Accounts;

final class Bkash extends Account
{
    public function getAccountId()
    {
        return 'guid';
    }
}
