<?php

namespace Shohoz\Griffin\Accounts;

final class CashOnHand extends EntityAccount
{
    public function getAccountId()
    {
        return 'guid';
    }

    public function getAccountSubType()
    {
        return AccountSubTypes::CASH_ON_HAND;
    }
}
