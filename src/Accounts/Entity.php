<?php

namespace Shohoz\Griffin\Accounts;

use Shohoz\Griffin\Contracts\Arrayable;

final class Entity implements Arrayable
{
    private $mobileNumber;
    private $accountName;
    private $accountSubType;

    public function __construct($mobileNumber, $accountName, $accountSubType)
    {
        $this->mobileNumber = $mobileNumber;
        $this->accountName = $accountName;
        $this->accountSubType = $accountSubType;
    }

    public function toArray()
    {
        return [
            'MobileNumber' => $this->mobileNumber,
            'AccountName' => $this->accountName,
            'AccountSubType' => $this->accountSubType
        ];
    }
}
