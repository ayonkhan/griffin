<?php

namespace Shohoz\Griffin\Accounts;

abstract class EntityAccount extends Account
{
    /** @var Entity */
    protected $entity;

    public function __construct($amount, $mobileNumber, $accountName)
    {
        parent::__construct($amount);
        $this->entity = new Entity($mobileNumber, $accountName, $this->getAccountSubType());
    }

    abstract function getAccountSubType();

    public function toArray()
    {
        return parent::toArray() + [
            'Entity' => $this->entity->toArray()
        ];
    }
}
