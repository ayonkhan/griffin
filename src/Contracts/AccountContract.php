<?php

namespace Shohoz\Griffin\Contracts;

interface AccountContract extends Arrayable
{
    public function getAccountId();
    public function getDisplayName();
    public function getAmount();
}
