<?php

namespace Shohoz\Griffin\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed push(array $debits, array $credits, $description = null, $timestamp = null)
 *
 * @see \Shohoz\Griffin\GriffinClient
 */
class Griffin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'griffin';
    }
}
