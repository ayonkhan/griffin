<?php

namespace Shohoz\Griffin;

use Carbon\Carbon;
use Shohoz\Griffin\Models\GriffinMongo;
use Symfony\Component\HttpFoundation\Response;

class GriffinClient
{
    public function push(array $debits, array $credits, $description = null, Carbon $timestamp = null)
    {
        if (!config('griffin.enabled')) {
            throw new \Exception('Griffin service is not enabled!');
        }

        $timestamp = is_null($timestamp) ? Carbon::now() : $timestamp;
        $data = [
            'CorrelationId' => $this->getGUID(),
            'Journal' => [
                'Debits' => $this->convertToArray($debits),
                'Credits' => $this->convertToArray($credits),
                'TxnDate' => $timestamp->toDateString(),
                'Description' => $description,
                'Source' => config('griffin.source_name')
            ]
        ];

        $options = [
            'url' => config('griffin.base_url'),
            'post' => true,
            'postfields' => json_encode($data),
            'httpheader' => [
                'Authorization:Bearer ' . $this->getBearerToken(),
                'Content-Type:application/json'
            ]
        ];

        $response = $this->curlExec($options);
        $data['response'] = json_encode($response);

        return GriffinMongo::create($data);
    }

    private function getGUID()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((float) microtime() * 10000); //optional for php 4.2.0 and up.
            $charId = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = substr($charId, 0, 8) . $hyphen
                . substr($charId, 8, 4) . $hyphen
                . substr($charId, 12, 4) . $hyphen
                . substr($charId, 16, 4) . $hyphen
                . substr($charId, 20, 12);

            return $uuid;
        }
    }

    private function convertToArray(array $accounts)
    {
        foreach ($accounts as &$account) {
            /** @var \Shohoz\Griffin\Contracts\Arrayable $account */
            $account = $account->toArray();
        }

        return $accounts;
    }

    private function getBearerToken()
    {
        $response = $this->curlExec([
            'url' => 'https://iam-dev.shohoz.com/connect/token',
            'post' => true,
            'postfields' => http_build_query([
                'grant_type' => 'client_credentials',
                'audience' => 'shohoz.iam.truck',
                'client_id' => 'truck',
                'client_secret' => '21138937-05a8-4d8b-9f5b-cfeb1e2b6a70'
            ]),
            'httpheader' => [
                'Content-Type:application/x-www-form-urlencoded'
            ]
        ]);

        if ($response['success'] == true && Response::HTTP_OK == $response['code']) {
            $result = json_decode($response['response'], true);

            return $result['access_token'];
        } else {
            return 'Invalid Token';
        }
    }

    private function curlExec(array $options)
    {
        $curlOptions = [];

        if (!isset($options['returntransfer'])) {
            $options['returntransfer'] = true;
        }

        foreach ($options as $key => $value) {
            $option = 'CURLOPT_' . strtoupper($key);

            $curlOptions[constant($option)] = $value;
        }

        $ch = curl_init();

        curl_setopt_array($ch, $curlOptions);
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $response = [
                'success' => false,
                'code' => curl_errno($ch),
                'error' => curl_error($ch)
            ];
        } else {
            $response = [
                'success' => true,
                'code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
                'response' => $result
            ];
        }

        curl_close($ch);

        return $response;
    }
}
