<?php

namespace Shohoz\Griffin;

use Illuminate\Support\ServiceProvider;

class GriffinServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/griffin.php' => config_path('griffin.php'),
        ], 'config');

        $this->mergeConfigFrom(__DIR__ . '/../config/griffin.php', 'griffin');

        config(['database.connections.' . config('griffin.database_connection_name') => config('griffin.mongodb')]);
    }

    public function register()
    {
        $this->app->register(\Jenssegers\Mongodb\MongodbServiceProvider::class);

        $this->app->bind('griffin', function () {
            if (!in_array(config('griffin.source_name'), ['ride', 'food', 'truck', 'bus', 'launch', 'learn'])) {
                throw new \Exception('Invalid source name! Please specify correctly.');
            }
            return new GriffinClient();
        });
    }
}
