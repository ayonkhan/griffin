<?php

namespace Shohoz\Griffin\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GriffinMongo extends Eloquent
{
    public $fillable = [
        'transaction_datetime', 'debits', 'credits', 'description', 'status'
    ];

    const STATUS_SCHEDULED = 0;
    const STATUS_ON_PROCESS = 1;
    const STATUS_SENT = 2;
    const STATUS_FAILED = 3;

    public function __construct(array $attributes = [])
    {
        if (!isset($this->connection)) {
            $this->setConnection(config('griffin.database_connection_name'));
        }

        if (!isset($this->table)) {
            $this->setTable(config('griffin.table_name'));
        }

        parent::__construct($attributes);
    }
}
